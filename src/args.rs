use std::path::PathBuf;
use clap::{Parser, Subcommand};

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Cli {
    /// Path to a db
    #[clap(short, long, parse(from_os_str), value_name = "PATH")]
    pub db: Option<PathBuf>,

    /// Path to a config file
    #[clap(short, long, parse(from_os_str), value_name = "FILE")]
    pub config: Option<PathBuf>,

    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Init db
    Init {},
    /// Add a db entry
    Add {
        /// name of a db entry
        name: String,

        /// Description of a db entry
        #[clap(short, long)]
        description: Option<String>,

        /// Tag to add to a db entry
        #[clap(short, long, value_name = "TAGS", value_delimiter=',', use_value_delimiter = true)]
        tags: Vec<String>,

        /// Container to add a db entry to
        #[clap(short, long)]
        container: String,

        /// Generate barcode for a db entry
        #[clap(short, long)]
        barcode: bool,

        /// Print barcode for a db entry
        #[clap(short, long, requires = "barcode")]
        print: bool,

        /// Quantity
        #[clap(short, long, default_value_t = 1)]
        quantity: u64,
    },

    /// Move a db entry to a different container
    Mv {
        /// name of a db entry
        name: Option<String>,

        /// name of a container
        container: Option<String>
    },

    /// Search for a db entry
    Search {
        /// Full-text to search for
        fulltext: String,

        /// Tag of a db entry
        #[clap(short, long, value_name = "TAG")]
        tags: Vec<String>,

        /// Name of a db entry
        #[clap(short, long = "name", value_name = "NAME")]
        names: Vec<String>,
    },

    /// List the contents of a container
    Ls {
        /// Name of a container
        #[clap(default_value = "/")]
        container: String,

        /// List contents of a container recursively
        #[clap(short, long)]
        recursive: bool,
    },

    /// Remove a db entry
    Rm {
        /// Name of a db entry
        name: String,

        /// Delete contents of a container recursively
        #[clap(short, long)]
        recursive: bool,
    },

    /// Show contents of a db entry
    Show {
        /// Name of a db entry
        name: String,

        /// Show all db entries with the same name
        #[clap(short, long)]
        all: bool
    },

    /// Process photos to add to corresponding db entries based on a barcode
    BarcodePhotos {
        #[clap(parse(from_os_str), value_name = "FILE")]
        files: Vec<PathBuf>,

        /// Hard link files instead of copying
        #[clap(short, long, conflicts_with = "mv")]
        link: bool,

        /// Move files instead of copying
        #[clap(short, long, conflicts_with = "link")]
        mv: bool,
    },

    /// Modify an existing db entry
    Modify {
        /// Name of a db entry
        name: String,

        #[clap(subcommand)]
        command: ModifyCommands,
    },

    /// Print barcode for a db entry
    Print {
        /// Name of a db entry
        name: String,
    },
}

#[derive(Subcommand)]
pub enum ModifyCommands {
    /// Add a tag to a db entry
    AddTag {
        /// Tag
        tag: String,
    },

    /// Remove a tag from a db entry
    RmTag {
        /// Tag
        tag: String,
    },

    /// Add a file to a db entry
    AddFile {
        /// File
        #[clap(parse(from_os_str))]
        file: PathBuf,

        /// Name of a file to store in a db entry
        #[clap(short, long)]
        name: Option<String>,

        /// Hard link files instead of copying
        #[clap(short, long, conflicts_with = "mv")]
        link: bool,

        /// Move files instead of copying
        #[clap(short, long, conflicts_with = "link")]
        mv: bool,
    },

    /// Remove a file from a db entry
    RmFile {
        /// File
        name: String,
    },

    /// Increase quantity
    Inc {
        /// By
        by: Option<u64>
    },

    /// Decrease quantity
    Dec {
        /// By
        by: Option<u64>
    },

    /// Set quantity
    SetQuantity {
        /// Quantity
        quantity: u64,
    },

    /// Set description
    SetDescription {
        /// New description
        description: Option<String>
    },

    /// Rename db entry
    Rename {
        /// New name
        new_name: String,
    },
}

