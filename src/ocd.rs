use std::path::{Path, PathBuf};
use walkdir::WalkDir;
use std::fs::{read_to_string, write, create_dir, remove_dir_all, copy, hard_link, rename, remove_file};
use serde::{Serialize, Deserialize};
use fs_extra::dir::{move_dir, CopyOptions};
use std::fmt;
use std::io::{Error, ErrorKind};

#[derive(Serialize, Deserialize, Debug)]
pub struct OcdEntry {
    #[serde(skip)]
    path: PathBuf,
    pub tags: Vec<String>,
    pub barcode: Option<String>,
    pub description: Option<String>,
    pub quantity: u64,
    #[serde(skip, default)]
    pub level: usize,
    pub is_root: bool
}

pub struct OcdIter {
    iter: Box<dyn Iterator<Item = OcdEntry>>
}

impl Iterator for OcdIter {
    type Item = OcdEntry;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl OcdEntry {
    fn new_from_path(path: &Path, level: usize) -> Result<Self, std::io::Error> {
        let buf = read_to_string(path.join(".ocd"))?;
        let mut db_entry = toml::from_str::<Self>(&buf).expect("Failed to deserialize config.");
        db_entry.path = path.to_path_buf();
        db_entry.level = level;
        Ok(db_entry)
    }

    pub fn create(path: &Path) -> Result<Self, std::io::Error> {
        let db = Self {
            path: path.to_path_buf(),
            tags: vec![],
            barcode: None,
            description: None,
            quantity: 0,
            level: 0,
            is_root: true
        };
        db.save()?;
        Ok(db)
    }

    pub fn root_from_path(path: &Path) -> Result<Self, std::io::Error> {
        Self::new_from_path(path, 0)
    }

    pub fn new_entry(&self, name: String, description: Option<String>, tags: Vec<String>, barcode: Option<String>, quantity: u64) -> Self {
        Self {
            path: self.path.join(name),
            tags: tags,
            barcode: barcode,
            description: description,
            quantity: quantity,
            level: self.level + 1,
            is_root: false
        }
    }

    pub fn iter(&self, recursive: bool) -> OcdIter {
        let level = self.level;
        let mut wd = WalkDir::new(self.path.as_path());
        if ! recursive {
            wd = wd.max_depth(1)
        }
        let iter = wd.into_iter()
            .filter_map(|e| e.ok())
            .filter(|e| e.file_type().is_dir())
            .filter_map(move |e| Self::new_from_path(&e.path(), level + e.depth()).ok());
        OcdIter { iter: Box::new(iter) }
    }

    pub fn name(&self) -> &str {
        if self.is_root {
            "/"
        } else {
            self.path.file_name().unwrap().to_str().unwrap()
        }
    }

    pub fn container(&self) -> Option<Self> {
        if ! self.is_root {
            Some(Self::new_from_path(&self.path.parent()?, self.level - 1).ok()?)
        } else {
            None
        }
    }

    pub fn files(&self) -> Result<Vec<String>, std::io::Error> {
        Ok(self.path
           .read_dir()?
           .filter_map(|p| p.ok())
           .map(|p| p.path())
           .filter(|p| p.is_file() && ! p.ends_with(".ocd"))
           .map(|p| p.file_name().unwrap().to_str().unwrap().to_string())
           .collect())
    }

    pub fn path_to_file(&self, name: &str) -> PathBuf {
        self.path.join(name)
    }

    pub fn save(&self) -> Result<(), std::io::Error> {
        match create_dir(self.path.as_path()) {
            Err(e) if e.kind() != std::io::ErrorKind::AlreadyExists => { return Err(e) },
            _ => {}
        }

        let entry_str = toml::to_string::<Self>(self).expect("Failed to serialize db entry.");
        write(self.path.join(".ocd"), entry_str)?;

        Ok(())
    }

    pub fn remove(&self, recursive: bool) -> Result<(), std::io::Error> {
        if !recursive &&
            self.path
                .read_dir()?
                .filter_map(|p| p.ok())
                .map(|p| p.path())
                .filter(|p| p.is_dir())
                .next() != None {
            Err(Error::new(ErrorKind::DirectoryNotEmpty, "db entry not empty"))
        } else {
            remove_dir_all(self.path.as_path())?;
            Ok(())
        }
    }

    pub fn find_first(&self, name: &String) -> Option<Self> {
        if name == "/" {
            Some(Self::new_from_path(self.path.as_path(), 0).ok()?)
        } else {
            self.iter(true).find(|e| e.name().eq(name))
        }
    }

    pub fn find_all(&self, name: &String) -> Vec<Self> {
        if name == "/" {
            vec![Self::new_from_path(self.path.as_path(), 0).unwrap()]
        } else {
            self.iter(true).filter(|e| e.name().eq(name)).collect()
        }
    }

    pub fn find_barcode(&self, barcode: &String) -> Option<Self> {
        self.iter(true).find(|e| e.barcode.is_some() && e.barcode.clone().unwrap().eq(barcode))
    }

    pub fn move_to(&mut self, container: Self) {
        let new_path = container.path.join(self.name());
        move_dir(self.path.as_path(), &container.path, &CopyOptions::new()).unwrap();
        self.path = new_path;
    }

    pub fn find_root(&self) -> PathBuf {
        let mut e = self.container().unwrap();

        while ! e.is_root {
            e = e.container().unwrap();
        }

        e.path
    }

    pub fn path(&self) -> PathBuf {
        let root = self.find_root();

        self.path.strip_prefix(root).unwrap().to_path_buf()
    }

    pub fn copy_file(&self, path: &Path, name: &str) -> Result<(), std::io::Error> {
        copy(path, self.path.join(name))?;
        Ok(())
    }

    pub fn hardlink_file(&self, path: &Path, name: &str) -> Result<(), std::io::Error> {
        hard_link(path, self.path.join(name))
    }

    pub fn move_file(&self, path: &Path, name: &str) -> Result<(), std::io::Error> {
        rename(path, self.path.join(name))
    }

    pub fn remove_file(&self, name: &str) -> Result<(), std::io::Error> {
        remove_file(self.path.join(name))
    }

    pub fn rename(&mut self, name: &str) -> Result<(), std::io::Error> {
        if self.is_root {
            return Err(Error::new(ErrorKind::Other, "Cannot rename root"))
        }
        let new_path = self.path.parent().unwrap().join(name);
        rename(self.path.as_path(), new_path.clone())?;
        self.path = new_path;
        Ok(())
    }
}

impl fmt::Display for OcdEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "/{}", self.path().display())?;
        writeln!(f, " - Name: {}", self.name())?;
        if let Some(description) = &self.description {
            writeln!(f, " - Description: {}", description)?;
        }
        if let Some(barcode) = &self.barcode {
            writeln!(f, " - Barcode: {}", barcode)?;
        }
        if self.quantity > 0 {
            writeln!(f, " - Quantity: {}", self.quantity)?;
        }
        if self.tags.len() > 0 {
            writeln!(f, " - Tags: {}", self.tags.join(" "))?;
        }
        if let Ok(files) = self.files() {
            if files.len() > 0 {
                writeln!(f, " - Files:")?;
                for file in files {
                    writeln!(f, "   - {} : {}", file, self.path_to_file(&file).display())?;
                }
            }
        }
        Ok(())
    }
}
