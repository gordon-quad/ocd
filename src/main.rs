#![feature(io_error_more)]
#![feature(int_roundings)]

mod ocd;
mod args;
mod config;
mod print;

use std::fs;
use dirs::{config_dir, data_local_dir};
use clap::Parser;
use std::io::{Cursor, stdin, stdout, Write};
use std::path::PathBuf;
use rand::Rng;

use qrcode::{QrCode, Version, EcLevel};
use image::{Luma, DynamicImage, GenericImageView};
use zbar_rust::ZBarImageScanner;

use args::{Cli, Commands, ModifyCommands};
use ocd::OcdEntry;
use config::Config;
use print::print_bytes;


const BARCODE_ALNUM: [char; 32] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                   'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                                   'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                                   'U', 'V'];

type BarcodeInt = u64;

fn barcode_to_string(barcode: BarcodeInt) -> String {
    let mut bc = barcode;
    let mut result = String::new();

    for _ in 0..(BarcodeInt::BITS.div_ceil(5)) {
        result.push(BARCODE_ALNUM[(bc & 31) as usize]);
        bc = bc >> 5;
    };

    result
}

fn print_barcode(config: &Config, barcode: &String) {
    let code = QrCode::with_version(barcode, Version::Normal(1), EcLevel::M).unwrap();
    let image = DynamicImage::ImageLuma8(code.render::<Luma<u8>>().build());
    let mut bytes: Vec<u8> = Vec::new();
    image.write_to(&mut Cursor::new(&mut bytes), image::ImageOutputFormat::Png).unwrap();

    print_bytes(&bytes, &config.printer, &config.print_options.clone().unwrap_or(vec![]));
}

fn add(config: &Config, db: &OcdEntry, name: &String, description: &Option<String>, tags: &Vec<String>, container: &String, barcode: bool, print: bool, quantity: u64) {
    let ocd_container = db.find_first(&container).expect("Container not found.");

    let bc = match barcode {
        true => {
            let mut rng = rand::thread_rng();
            let r: BarcodeInt = rng.gen();
            Some(format!("OCD:{}", barcode_to_string(r)))
        },
        false => None
    };

    if print {
        print_barcode(config, &bc.clone().unwrap());
    };

    let ocd_entry = ocd_container.new_entry(name.clone(), description.clone(), tags.to_vec(), bc, quantity);
    ocd_entry.save().expect("Save failed.");
}

fn rm(_config: &Config, db: &OcdEntry, name: String, recursive: bool) {
    let ocd_entry = db.find_first(&name).expect("Db entry not found.");

    ocd_entry.remove(recursive).expect("Cannot delete db entry.");
}

fn mv(_config: &Config, db: &OcdEntry, name: &Option<String>, container: &Option<String>) {
    let mut entry = match name {
        Some(name) => db.find_first(&name).expect("Cannot find db entry"),
        None => {
            print!("Enter db entry's barcode: ");
            stdout().flush().unwrap();
            let mut buf = String::new();
            stdin().read_line(&mut buf).expect("Cannot read barcode from stdin.");
            db.find_barcode(&buf.trim().to_string()).expect(&format!("Cannot find barcode {} in db.", &buf))
        }
    };

    let container = match container {
        Some(container) => db.find_first(&container).expect("Cannot find container."),
        None => {
            print!("Enter container's barcode: ");
            stdout().flush().unwrap();
            let mut buf = String::new();
            stdin().read_line(&mut buf).expect("Cannot read barcode from stdin.");
            db.find_barcode(&buf.trim().to_string()).expect("Cannot find barcode in db.")
        }
    };

    entry.move_to(container);
}

fn ls(_config: &Config, db: &OcdEntry, container: &String, recursive: bool) {
    let entry = db.find_first(&container).expect("Cannot find container.");
    let root_lvl = entry.level;
    for e in entry.iter(recursive) {
        println!("{0:1$}- {2}", " ", (e.level - root_lvl)*2+1, e.name());
    }
}

fn show(_config: &Config, db: &OcdEntry, name: &String, all: bool) {
    let entries = if ! all {
        vec![db.find_first(name)]
            .into_iter()
            .filter_map(|e| e)
            .collect()
    } else {
        db.find_all(name)
    };

    if entries.len() > 0 {
        for e in entries {
            println!("{}", e);
        }
    } else {
        eprintln!("Nothing found");
    }
}

fn process_photos(_config: &Config, db: &OcdEntry, files: &Vec<PathBuf>, link: bool, mv: bool) {
    'files: for f in files {
        if ! f.exists() {
            eprintln!("{}: File not found", f.display());
            continue;
        }

        if ! f.is_file() {
            eprintln!("{}: File not a regular file", f.display());
            continue;
        }

        match image::open(f) {
            Ok(img) => {
                let (width, height) = img.dimensions();

                let luma_img = img.to_luma8();

                let luma_img_data: Vec<u8> = luma_img.to_vec();

                let mut scanner = ZBarImageScanner::new();

                let results = scanner.scan_y800(&luma_img_data, width, height).unwrap();

                for result in results {
                    if let Ok(barcode) = String::from_utf8(result.data) {
                        println!("{}", barcode);
                        if let Some(entry) = db.find_barcode(&barcode) {
                            let result = match (link, mv) {
                                (true, false) => entry.hardlink_file(f, "photo.png"),
                                (false, true) => entry.move_file(f, "photo.png"),
                                _ => entry.copy_file(f, "photo.png")
                            };
                            if let Err(e) = result {
                                eprintln!("{}: {}", f.display(), e);
                            } else {
                                println!("{}: Added to /{}", f.display(), entry.path().display());
                            }
                            continue 'files;
                        }
                    }
                }

                eprintln!("{}: Cannot find suitable barcode", f.display())
            },
            Err(e) => {
                eprintln!("{}: {}", f.display(), e);
                continue;
            }
        }
    }
}

fn modify<F: Fn(&mut OcdEntry) -> Result<(), std::io::Error>>(_config: &Config, db: &OcdEntry, name: &String, func: F) {
    let mut entry = db.find_first(name).expect("Db entry not found");
    func(&mut entry).expect("Failed to modify db entry");
    entry.save().expect("Cannot save db entry");
}

fn rename(_config: &Config, db: &OcdEntry, name: &String, new_name: &String) {
    let mut entry = db.find_first(name).expect("Db entry not found");
    entry.rename(new_name).expect("Cannot rename");
}

fn add_file(_config: &Config, db: &OcdEntry, name: &String, file: &PathBuf, filename: &Option<String>, link: bool, mv: bool) {
    let entry = db.find_first(name).expect("Db entry not found");
    let filename = filename.clone().unwrap_or(file.file_name().unwrap().to_str().unwrap().to_string());
    match (link, mv) {
        (true, false) => entry.hardlink_file(file, &filename).expect("Cannot hardlink file"),
        (false, true) => entry.move_file(file, &filename).expect("Cannot move file"),
        _ => entry.copy_file(file, &filename).expect("Cannot copy file")
    };
}

fn rm_file(_config: &Config, db: &OcdEntry, name: &String, filename: &String) {
    let entry = db.find_first(name).expect("Db entry not found");
    entry.remove_file(filename).expect("Cannot remove file");
}

fn main() {
    let cli = Cli::parse();

    let default_config_dir = config_dir().unwrap().join("ocd");
    let default_config_file = default_config_dir.join("config.toml");
    let default_db_path = data_local_dir().unwrap().join("ocd");

    if !default_config_file.exists() {
        if !default_config_dir.exists() {
            fs::create_dir(default_config_dir).expect("Cannot create config dir");
        }

        let default_config_str = toml::to_string::<Config>(&(Config::default())).expect("Failed to serialize config.");
        fs::write(default_config_file.as_path(), default_config_str).expect("Failed to write default config.");
    }

    let config_file = cli.config.unwrap_or(default_config_file);

    let buf = fs::read_to_string(config_file).unwrap();

    let config = toml::from_str::<Config>(&buf).expect("Failed to deserialize config.");

    let db_path = cli.db.unwrap_or(config.db.clone().unwrap_or(default_db_path));
    let db = match &cli.command {
        Commands::Init {} => OcdEntry::create(db_path.as_path()).expect("db init failed"),
        _ => OcdEntry::root_from_path(db_path.as_path()).expect("db init failed")
    };

    match &cli.command {
        Commands::Init {} => {
        }
        Commands::Add { name, description, tags, container, barcode, print, quantity } => {
            add(&config, &db, name, description, tags, container, *barcode, *print, *quantity);
        },
        Commands::Mv { name, container } => {
            mv(&config, &db, name, container);
        },
        Commands::Search { fulltext, tags, names } => {
            println!("Search {:?} {:?} {:?}", fulltext, tags, names);
        },
        Commands::Ls { container, recursive } => {
            ls(&config, &db, container, *recursive);
        },
        Commands::Rm { name, recursive } => {
            rm(&config, &db, name.clone(), *recursive);
        },
        Commands::Show { name, all } => {
            show(&config, &db, name, *all);
        },
        Commands::Print { name } => {
            let entry = db.find_first(name).expect("Db entry not found");
            let barcode = entry.barcode.expect("No barcode for a db entry.");
            print_barcode(&config, &barcode);
        },
        Commands::BarcodePhotos { files, link, mv } => {
            process_photos(&config, &db, files, *link, *mv);
        },
        Commands::Modify { name, command } => {
            match command {
                ModifyCommands::AddTag { tag } => {
                    modify(&config, &db, name, |e| {
                        e.tags.push(tag.clone());
                        e.tags.sort_unstable();
                        e.tags.dedup();
                        Ok(())
                    });
                },
                ModifyCommands::RmTag { tag } => {
                    modify(&config, &db, name, |e| {
                        e.tags = e.tags
                            .clone()
                            .into_iter()
                            .filter(|t| ! t.eq(tag))
                            .collect();
                        Ok(())
                    });
                },
                ModifyCommands::AddFile { file, name: filename, link, mv } => {
                    add_file(&config, &db, name, file, filename, *link, *mv);
                },
                ModifyCommands::RmFile { name: filename } => {
                    rm_file(&config, &db, name, filename);
                },
                ModifyCommands::Inc { by } => {
                    modify(&config, &db, name, |e| {
                        e.quantity += by.unwrap_or(1);
                        Ok(())
                    });
                },
                ModifyCommands::Dec { by } => {
                    modify(&config, &db, name, |e| {
                        e.quantity -= by.unwrap_or(1);
                        Ok(())
                    });
                },
                ModifyCommands::SetQuantity { quantity } => {
                    modify(&config, &db, name, |e| {
                        e.quantity = *quantity;
                        Ok(())
                    });
                },
                ModifyCommands::SetDescription { description } => {
                    modify(&config, &db, name, |e| {
                        e.description = description.clone();
                        Ok(())
                    });
                },
                ModifyCommands::Rename { new_name } => {
                    rename(&config, &db, name, new_name);
                },
            }
        },
    }
}
