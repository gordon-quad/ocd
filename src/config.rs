use std::path::PathBuf;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub db: Option<PathBuf>,
    pub paper: Option<String>,
    pub printer: Option<String>,
    pub print_options: Option<Vec<String>>,
    pub barcode: Option<String>,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            db: None,
            paper: None,
            printer: None,
            print_options: None,
            barcode: None
        }
    }
}
