use std::process::{Command, Stdio};
use std::io::Write;

/**
 * Print using lp
 */
pub fn print_bytes(buf: &[u8], printer_name: &Option<String>, options: &Vec<String>) -> bool {

    let mut args: Vec<&str> = options.into_iter().flat_map(|e| ["-o", e]).collect();

    if let Some(name) = printer_name {
        args.push("-d");
        args.push(name);
    };

    let mut child = Command::new("lp")
        .args(args)
        .stdin(Stdio::piped())
        .spawn().unwrap();

    let mut stdin = child.stdin.take().unwrap();

    stdin.write_all(buf).unwrap();
    drop(stdin);
    let process = child.wait_with_output().unwrap();

    return process.status.success();
}

