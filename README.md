# Object Collection Discoverer
Inventory system for cli-enabled hoarders.

# Dependencies
- [zbar](https://github.com/mchehab/zbar)

# Installation
```
cargo install --path .
```

# Usage

```
$ ocd --help
```

## Init db
```
$ ocd init
```

## Add a container
```
$ ocd add junk_box --container / --description "Box for junk" --tags junk,box
```

## Add an entry and print a barcode for it
```
$ ocd add junk --barcode --print --container junk_box --description "This is very important junk" --tags junk,garbage
```

## List contents of a container
```
$ ocd ls junk_box
 - junk_box
   - junk
```

## Show an entry
```
$ ocd show junk
/junk_box/junk
 - Name: junk
 - Description: This is very important junk
 - Barcode: OCD:A50JVK1AF27LE
 - Quantity: 1
 - Tags: junk garbage
```

## Add a photo of an item that contains a barcode label to that item
```
$ ocd barcode-photos photo1.png photo2.png
```

## Move item from one container to another
```
$ ocd mv junk other_junk_box
```

## Move item from one cntainer to another using barcode reader
```
$ ocd mv
Enter db entry's barcode: OCD:SHH7RUND9JARB
Enter container's barcode: OCD:A50JVK1AF27LE
```

## Add a file to an entry
```
$ ocd modify junk add-file /path/to/foo.pdf datasheet.pdf
```

